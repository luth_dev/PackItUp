#include <QCoreApplication>
#include <QDirIterator>
#include <QDir>
#include <QDebug>
#include <QTemporaryFile>
#include <QTextStream>
#include <QStringListIterator>

bool ValidPerm(QString path) // Validate path write permission
{
    QTemporaryFile file(path + "/XXXXXX"); // Create temporary file
    if (file.open()) // Check if temporary file created successfully
    {
        qDebug().noquote().nospace() << "Temporary file " << file.fileName() << " created!";
        return true;
    }
    else
        return false;

}

QStringList GetExcludeList(QString path)
{
    qDebug() << "In GetExcludeList";
    QStringList tmp;
    tmp << "exclude" << "backup"; // Exclude the exclude file duh
    QFile exclude(path + "/exclude");
    if (exclude.exists()) // Check if exclude file exists
    {
        if (exclude.open(QIODevice::ReadOnly)) // Check if file can be opened (prevents undefined behavior if file exists but user can't read it)
        {
            QTextStream fin(&exclude);
            while(!fin.atEnd())
                tmp << fin.readLine();
        }
        else
            qDebug() << "Can't open exclude file!";
    }
    else
        qDebug() << "Exclude file doesn't exist!";
    qDebug() << "Done GetExcludeList";
    return tmp;
}

QStringList CreateFileList(QString path)
{
    qDebug() << "In CreateFileList";
    QStringList tmp;
    QStringList exclude(GetExcludeList(path));
    QDirIterator it(path, exclude, QDir::Files | QDir::NoDotAndDotDot | QDir::CaseSensitive );
    while(it.hasNext())
    {
        qDebug() << it.next();
    }
    qDebug() << "Done CreateFileList";
    return tmp;
}

void CreateHashFile(QString path) //
{
    qDebug() << "In CreateHashFile";
    QStringList list = CreateFileList(path);
    QStringListIterator it(list);
    while(it.hasNext())
        qDebug() << it.next().toLocal8Bit().constData();
    qDebug() << "Done CreateHashFile";
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QString root = QDir::currentPath();
    if (argc > 1) // Check if command arguments exist
    {
        if (strcmp(argv[1], "-p") == 0) // Check if path switch exists
        {
            if (argc > 2) // Check if path argument exists
            {
                if (QDir(argv[2]).exists()) // Check if path exists
                {
                    if (ValidPerm(QString(argv[2])))
                        root = argv[2];
                    else
                        qDebug() << "Folder not writtable! Using current directory";
                }
                else
                    qDebug() << "Path doesn't exit! Using current directory";
            }
            else
                qDebug() << "No path parameter found! Using current directory";
        }
        else
            qDebug() << "Unknown arguments!";
    }
    else
        qDebug() << "No path override found! Using current directory";
    qDebug().noquote().nospace() << "Path: " << root;
    QTextStream in(stdin);
    char x;
    qDebug() << "Using path: " << root << " \nContinue?(Y/N): ";
    in >> x;
    if (x == 'y' || x == 'Y')
    {
        qDebug() << "x if";
        CreateHashFile(root);
    }
    else
        a.exit(0);
    return a.exec();
}
